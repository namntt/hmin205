package com.example.smartcity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.smartcity.data.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener{
    EditText uname, pw, confirmPw, email;
    Button signup, cancel;
    ProgressBar progressBar;
    FirebaseAuth fbAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        fbAuth = FirebaseAuth.getInstance();
        initialize();
    }

    private void signupUser() {
        String txtEmail = email.getText().toString().trim();
        String txtPw = pw.getText().toString().trim();
        String txtConfirmPw = confirmPw.getText().toString().trim();
        String txtUsername = uname.getText().toString().trim();

        if(TextUtils.isEmpty(txtEmail) || TextUtils.isEmpty(txtPw) ||
                TextUtils.isEmpty(txtConfirmPw) || TextUtils.isEmpty(txtUsername)){
            Toast.makeText(this, "All fields must not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if(txtPw.length() < 6){
            Toast.makeText(this, "password too short", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
        }

        if(txtPw.equals(txtConfirmPw)){
            progressBar.setVisibility(View.VISIBLE);
            fbAuth.createUserWithEmailAndPassword(txtEmail, txtPw)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), "Registration successful!", Toast.LENGTH_LONG).show();
                                progressBar.setVisibility(View.GONE);
                                finish();
                            }
                            else {
                                Toast.makeText(getApplicationContext(), "Registration failed! Please try again", Toast.LENGTH_LONG).show();
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                    });
        }
    }

    @Override
    public void onClick(View view){
        if (view == cancel){
            finish();
        }
        else if (view == signup) {
            signupUser();
        }
    }

    public void initialize(){
        uname = findViewById(R.id.signup_uname);
        pw = findViewById(R.id.signup_password);
        confirmPw = findViewById(R.id.signup_confirm);
        email = findViewById(R.id.signup_email);

        signup = findViewById(R.id.btn_signup);
        cancel = findViewById(R.id.btn_signup_cancel);

        progressBar = findViewById(R.id.progressBar3);
        progressBar.setVisibility(View.GONE);

        signup.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }
}
