package com.example.smartcity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginFragment extends Fragment {
    String TAG = "com.example.smartcity";
    TextView signupLink;
    View view;
    Button btnLogin;
    ProgressBar progressBar;
    Intent intent;
    FirebaseAuth fbAuth;
    DatabaseReference dbRef;

    public LoginFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fbAuth = FirebaseAuth.getInstance();
        if (fbAuth.getCurrentUser() != null) {
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
        }

        view = inflater.inflate(R.layout.fragment_login, container, false);
        fbAuth = FirebaseAuth.getInstance();

        btnLogin = view.findViewById(R.id.btn_login);
        progressBar = view.findViewById(R.id.progressBar);
        signupLink = view.findViewById(R.id.signup_link);

        progressBar.setVisibility(View.GONE);

        signupLink.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), SignupActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                EditText email = getView().findViewById(R.id.login_email);
                EditText pw = getView().findViewById(R.id.login_password);
                String txtEmail = email.getText().toString().trim();
                String txtPw = pw.getText().toString().trim();

                progressBar.setVisibility(View.VISIBLE);

                if(!TextUtils.isEmpty(txtEmail) || !TextUtils.isEmpty(txtPw)){
                    fbAuth.signInWithEmailAndPassword(txtEmail, txtPw).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(getContext(), "Login successful!", Toast.LENGTH_LONG).show();
                                progressBar.setVisibility(View.GONE);


                                intent = new Intent(getActivity(), MainActivity.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getContext(),
                                        task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                    });
                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "email or password must not be empty", Toast.LENGTH_SHORT).show();
                }

            }
        });
        return view;
    }

    public void checkUserExistance(){
        dbRef = FirebaseDatabase.getInstance().getReference().child("users");
        final String user_id = fbAuth.getCurrentUser().getUid();
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(user_id)){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getContext(),
                            "Authentication Success.", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), MainActivity.class));
                }else {
                    Toast.makeText(getActivity(), "User not registered!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
