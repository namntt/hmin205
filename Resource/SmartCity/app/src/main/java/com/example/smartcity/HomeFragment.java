package com.example.smartcity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.smartcity.R;
import com.example.smartcity.alarm.AlarmActivity;

public class HomeFragment extends Fragment {
    private Intent intent;
    private View view;
    private Button news, weather, traffic, calendar, alarm, todo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        weather = view.findViewById(R.id.btn_home_weather);
        news = view.findViewById(R.id.btn_home_news);
        traffic = view.findViewById(R.id.btn_home_traffic);
        calendar = view.findViewById(R.id.btn_home_calendar);
        alarm = view.findViewById(R.id.btn_home_alarm);
        todo = view.findViewById(R.id.btn_home_todo);

        weather.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), WeatherActivity.class);
                startActivity(intent);
            }
        });

        news.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), NewsActivity.class);
                startActivity(intent);
            }
        });

        traffic.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), TrafficActivity.class);
                startActivity(intent);
            }
        });

        calendar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), CalendarActivity.class);
                startActivity(intent);
            }
        });

        alarm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), AlarmActivity.class);
                startActivity(intent);
            }
        });

        todo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), TodoActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }
}
