package com.example.smartcity.weather;

import android.content.Context;
import android.util.Log;

import com.example.smartcity.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RemoteFetch {
    private static String TAG = "Fetching...";
    private static final String OPEN_WEATHER_MAP_API =
            "http://api.openweathermap.org/data/2.5/weather?q=%s";

    public static JSONObject getJSON(Context context, String city){
        try {
            URL url = new URL(String.format(OPEN_WEATHER_MAP_API, city));
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestProperty("x-api-key",
                    context.getString(R.string.open_weather_maps_app_id));
            Log.d(TAG, "getJSON: " + connection.getResponseMessage());
            InputStream inputStream = connection.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder(1024);
            String line = reader.readLine();
            Log.d(TAG, "getJSON: " + line);
            while (line != null)
            {
                stringBuilder.append(line);
            }

            JSONObject data = new JSONObject(stringBuilder.toString());

            // This value will be 404 if the request was not
            // successful
            if(data.getInt("cod") != 200){
                return null;
            }
            return data;
        }catch(Exception e){
            return null;
        }
    }
}
