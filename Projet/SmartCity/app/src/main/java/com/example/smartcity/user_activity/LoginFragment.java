package com.example.smartcity.user_activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartcity.MainActivity;
import com.example.smartcity.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginFragment extends Fragment {
    String TAG = "com.example.smartcity";
    TextView signupLink;
    TextInputEditText email, pw;
    View view;
    MaterialButton btnLogin;
    ProgressBar progressBar;
    Intent intent;
    FirebaseAuth fbAuth;
    FirebaseUser fbUser;

    public LoginFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);

        initialize(view);

        signupLink.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), SignupActivity.class);
                startActivity(intent);
            }
        });

        fbAuth = FirebaseAuth.getInstance();
        fbUser = fbAuth.getCurrentUser();

        if (fbUser != null) {
            fbUser = fbAuth.getCurrentUser();
            Toast.makeText(getContext(), "Already logged in as " + fbUser.getEmail(), Toast.LENGTH_LONG).show();
            intent = new Intent(getActivity(), MainActivity.class);
            startActivity(intent);
        } else {
            btnLogin.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    email = getView().findViewById(R.id.login_email);
                    pw = getView().findViewById(R.id.login_password);
                    String txtEmail = email.getText().toString().trim();
                    String txtPw = pw.getText().toString().trim();

                    progressBar.setVisibility(View.VISIBLE);
                    btnLogin.setEnabled(false);

                    if(!TextUtils.isEmpty(txtEmail) && !TextUtils.isEmpty(txtPw)){
                        fbAuth.signInWithEmailAndPassword(txtEmail, txtPw).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    progressBar.setVisibility(View.GONE);
                                    btnLogin.setEnabled(false);
                                    intent = new Intent(getActivity(), MainActivity.class);
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(getContext(),
                                            task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.GONE);
                                    btnLogin.setEnabled(true);
                                }
                            }
                        });
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "Email or Password must not be empty", Toast.LENGTH_SHORT).show();
                        btnLogin.setEnabled(true);
                    }
                }
            });
        }

        return view;
    }

    private void initialize(View view){
        btnLogin = view.findViewById(R.id.btn_login);
        progressBar = view.findViewById(R.id.progressBar);
        signupLink = view.findViewById(R.id.signup_link);

        progressBar.setVisibility(View.GONE);
    }
}
