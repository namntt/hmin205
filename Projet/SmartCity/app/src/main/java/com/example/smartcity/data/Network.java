package com.example.smartcity.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Network {
    private String networkName, privacy, category, createdDate, id;
    private Post post;

    public Network() {
    }

    public Network(String networkName, String privacy, String category, String id) {
        this.networkName = networkName;
        this.privacy = privacy;
        this.category = category;
        this.id = id;
        createdDate = new SimpleDateFormat("dd/MM/yyy - HH:mm:ss", Locale.FRANCE).format(Calendar.getInstance().getTime());
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
