package com.example.smartcity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.smartcity.data.Network;
import com.example.smartcity.data.Post;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class PostActivity extends AppCompatActivity {
    FloatingActionButton btn_send;
    ListView postList;
    MaterialTextView subpostSender, subpostDate, subpostContent;
    TextInputEditText text;
    MaterialTextView title;

    DatabaseReference dbRef;
    FirebaseDatabase fDb;
    FirebaseListAdapter<Post> adapter;

    Post post;

    String networkId, TAG = "SMARTCITY.POST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialize();
        displayPostList();
    }

    private void displayPostList() {
        Query query = dbRef.child(networkId).child("post");
        FirebaseListOptions<Post> options = new FirebaseListOptions.Builder<Post>().setQuery(query, Post.class).setLayout(R.layout.subpost_list).build();
        adapter = new FirebaseListAdapter<Post>(options) {
            @Override
            protected void populateView(@NonNull View v, @NonNull Post model, int position) {
                subpostSender = v.findViewById(R.id.subpost_sender);
                subpostDate = v.findViewById(R.id.subpost_date);
                subpostContent = v.findViewById(R.id.subpost_content);

                subpostContent.setText(model.getContent());
                subpostDate.setText(model.getSendDate());
                subpostSender.setText(model.getSender());
            }
        };
        postList.setAdapter(adapter);

    }

    public void initialize() {
        btn_send = findViewById(R.id.btn_send);
        postList = findViewById(R.id.post_subpost_list);
        text = findViewById(R.id.post_text);
        title = findViewById(R.id.activityTitle);

        Intent intent = getIntent();
        networkId = intent.getStringExtra("networkKey");

//        Toast.makeText(PostActivity.this, networkId, Toast.LENGTH_LONG).show();

        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference().child("networks");

        dbRef.child(networkId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                title.setText(dataSnapshot.child("networkName").getValue(String.class));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "Error: " + databaseError.toString());
            }
        });
    }
}
