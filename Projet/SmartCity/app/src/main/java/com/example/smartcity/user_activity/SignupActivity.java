package com.example.smartcity.user_activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.smartcity.R;
import com.example.smartcity.data.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener{
    String TAG = "smartcity.signup";
    TextInputEditText fname, pw, confirmPw, email;
    MaterialButton signup, cancel;
    ProgressBar progressBar;
    FirebaseAuth fbAuth;
    DatabaseReference dbRef;
    FirebaseDatabase fDb;
    FirebaseUser fbUser;

    User user;
    String userId, txtEmail, txtPw, txtConfirmPw, txtFullname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        fbAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference("users");

        initialize();
    }

    private void signupUser() {
        txtEmail = email.getText().toString().trim();
        txtPw = pw.getText().toString().trim();
        txtConfirmPw = confirmPw.getText().toString().trim();
        txtFullname = fname.getText().toString().trim();

        if(TextUtils.isEmpty(txtEmail) || TextUtils.isEmpty(txtPw) ||
                TextUtils.isEmpty(txtConfirmPw) || TextUtils.isEmpty(txtFullname)){
            Toast.makeText(this, "All fields must not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if(txtPw.length() < 6){
            Toast.makeText(this, "password too short", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
        }

        if(txtPw.equals(txtConfirmPw)){
            user = new User(txtFullname, txtEmail, txtPw);

            progressBar.setVisibility(View.VISIBLE);

            dbRef.orderByChild("email").equalTo(user.getEmail()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()){
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Email existed, please try again", Toast.LENGTH_LONG).show();
                    } else {
                        fbAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
                                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(getApplicationContext(), "Registration successful!", Toast.LENGTH_LONG).show();
                                            progressBar.setVisibility(View.GONE);
                                            if (TextUtils.isEmpty(userId)) {
                                                userId = dbRef.push().getKey();
                                            }
                                            fbUser = fbAuth.getCurrentUser();
                                            if (fbUser != null) {
                                                userId = fbUser.getUid();
                                                dbRef.child(userId).setValue(user);
                                                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                                        .setDisplayName(txtFullname).build();

                                                fbUser.updateProfile(profileUpdates)
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()) {
                                                                    Log.d(TAG, "User profile updated.");
                                                                }
                                                            }
                                                        });
                                                finish();
                                            }
                                        }
                                        else {
                                            Toast.makeText(getApplicationContext(), "Registration failed! Please try again", Toast.LENGTH_LONG).show();
                                            progressBar.setVisibility(View.GONE);
                                        }
                                    }
                                });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {}
            });
        }
    }

    @Override
    public void onClick(View view){
        if (view == cancel){
            finish();
        }
        else if (view == signup) {
            signupUser();
        }
    }

    public void initialize(){
        fname = findViewById(R.id.signup_fname);
        pw = findViewById(R.id.signup_password);
        confirmPw = findViewById(R.id.signup_confirm);
        email = findViewById(R.id.signup_email);

        signup = findViewById(R.id.btn_signup);
        cancel = findViewById(R.id.btn_signup_cancel);

        progressBar = findViewById(R.id.progressBar3);
        progressBar.setVisibility(View.GONE);

        signup.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }
}
