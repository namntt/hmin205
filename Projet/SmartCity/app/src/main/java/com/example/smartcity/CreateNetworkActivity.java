package com.example.smartcity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.smartcity.data.Network;
import com.example.smartcity.data.Post;
import com.example.smartcity.user_activity.ProfileActivity;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CreateNetworkActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener{
    List<String> privacyList, categories;
    ArrayAdapter<String> adapterPrivacy, adapterCategory;
    String networkId, subPostId;

    TextInputEditText networkName;
    MaterialButton btn_create, btn_cancel;
    Spinner privacy, category;
    ProgressBar progressBar;

    Network network;
    Post post, post1;

    FirebaseAuth fbAuth;
    DatabaseReference dbRef;
    FirebaseDatabase fDb;
    FirebaseUser fbUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_network);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialize();
    }

    public void createNetwork() {
        String txtName = networkName.getText().toString().trim();
        String txtPrivacy = privacy.getSelectedItem().toString().trim();
        String txtCategory = category.getSelectedItem().toString().trim();

        if (TextUtils.isEmpty(networkId)) {
            networkId = dbRef.push().getKey();
        }

        if (TextUtils.isEmpty(subPostId)) {
            subPostId = "subpost " + dbRef.child("post").push().getKey();
        }

        if(TextUtils.isEmpty(txtName)){
            Toast.makeText(this, "Network name must not be empty", Toast.LENGTH_SHORT).show();
            return;
        } else {
            network = new Network(txtName, txtPrivacy, txtCategory, networkId);
            post = new Post("Welcome", fbUser.getDisplayName());
            dbRef.child(networkId).setValue(network);
            dbRef.child(networkId).child("post").child(subPostId).setValue(post);

            Toast.makeText(CreateNetworkActivity.this, "Network created!", Toast.LENGTH_LONG).show();
        }
        finish();
    }

    @Override
    public void onClick(View view){
        if (view == btn_cancel) {
            String message = "Continue creating group?";
            AlertDialog.Builder builder = new AlertDialog.Builder(CreateNetworkActivity.this);
            builder
                    .setMessage(message)
                    .setPositiveButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    })
                    .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }
        else if (view == btn_create) {
            createNetwork();
        }
    }

    public void initialize() {
        btn_cancel = findViewById(R.id.btn_network_cancel);
        btn_create = findViewById(R.id.btn_network_create);
        privacy = findViewById(R.id.network_privacy);
        networkName = findViewById(R.id.network_name);
        category = findViewById(R.id.network_category);
        progressBar = findViewById(R.id.progressBar3);
        progressBar.setVisibility(View.GONE);

        privacyList = new ArrayList<>();
        privacyList.add("Public");
        privacyList.add("Private");

        categories = new ArrayList<>();
        categories.add("Sports");
        categories.add("Locals");
        categories.add("Fashions");
        categories.add("Culture");
        categories.add("Music/Art");
        categories.add("Politics");
        categories.add("Others");

        adapterPrivacy = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, privacyList);
        adapterPrivacy.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        privacy.setAdapter(adapterPrivacy);

        adapterCategory = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);
        adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(adapterCategory);

        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference().child("networks");
        fbAuth = FirebaseAuth.getInstance();
        fbUser = fbAuth.getCurrentUser();

        btn_cancel.setOnClickListener(this);
        btn_create.setOnClickListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {}

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}
}
