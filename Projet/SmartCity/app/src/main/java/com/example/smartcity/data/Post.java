package com.example.smartcity.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Post {
    private String content, sendDate, sender;

    public Post() {
    }

    public Post(String content, String sender) {
        this.content = content;
        this.sender = sender;
        sendDate = new SimpleDateFormat("dd/MM/yyy - HH:mm:ss", Locale.FRANCE).format(Calendar.getInstance().getTime());
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
