package com.example.smartcity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.smartcity.alarm.AlarmActivity;
import com.example.smartcity.news.NewsActivity;
import com.example.smartcity.weather.WeatherActivity;
import com.example.smartcity.weather.WeatherFragment;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class HomeFragment extends Fragment {
    private Intent intent;
    private View view;
    private MaterialButton news, weather, social, calendar, alarm, todo;

    private FirebaseAuth fbAuth;
    DatabaseReference dbRef;
    FirebaseDatabase fDb;
    private FirebaseUser fbUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        fbAuth = FirebaseAuth.getInstance();
        fbUser = fbAuth.getCurrentUser();

        weather = view.findViewById(R.id.btn_home_weather);
        news = view.findViewById(R.id.btn_home_news);
        social = view.findViewById(R.id.btn_home_social);
        calendar = view.findViewById(R.id.btn_home_calendar);
        alarm = view.findViewById(R.id.btn_home_alarm);
        todo = view.findViewById(R.id.btn_home_todo);

        weather.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), WeatherActivity.class);
                startActivity(intent);
            }
        });

        news.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), NewsActivity.class);
                startActivity(intent);
            }
        });

        social.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), SocialActivity.class);
                startActivity(intent);
            }
        });

        calendar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), CalendarActivity.class);
                startActivity(intent);
            }
        });

        alarm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), AlarmActivity.class);
                startActivity(intent);
            }
        });

        todo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(getActivity(), TodoActivity.class);
                startActivity(intent);
            }
        });

        if (fbUser == null) {
            weather.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    Toast.makeText(getContext(), "Please log in first", Toast.LENGTH_SHORT).show();
                }
            });

            news.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    Toast.makeText(getContext(), "Please log in first", Toast.LENGTH_SHORT).show();
                }
            });

            social.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    Toast.makeText(getContext(), "Please log in first", Toast.LENGTH_SHORT).show();
                }
            });

            calendar.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    Toast.makeText(getContext(), "Please log in first", Toast.LENGTH_SHORT).show();
                }
            });

            alarm.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    Toast.makeText(getContext(), "Please log in first", Toast.LENGTH_SHORT).show();
                }
            });

            todo.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    Toast.makeText(getContext(), "Please log in first", Toast.LENGTH_SHORT).show();
                }
            });
        }

        return view;
    }
}
