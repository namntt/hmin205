package com.example.smartcity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.renderscript.Sampler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.smartcity.data.Network;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.datepicker.MaterialTextInputPicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class SocialActivity extends AppCompatActivity implements View.OnClickListener {
    String TAG = "SMARTCITY.SOCIAL";
    FloatingActionButton btn_add, btn_search;
    ListView networkList;
    MaterialTextView networkName, networkCategory, networkPrivacy;
    TextInputEditText searchField;
    MaterialButton btn_join;
    Intent intent;

    DatabaseReference dbRef;
    FirebaseDatabase fDb;
    FirebaseListAdapter<Network> adapter;

    String networkId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialize();
        displayNetworkList();
    }

    private void displayNetworkList() {
        FirebaseListOptions<Network> options = new FirebaseListOptions.Builder<Network>().setQuery(dbRef, Network.class).setLayout(R.layout.network_list).build();
        adapter = new FirebaseListAdapter<Network>(options) {
            @Override
            protected void populateView(@NonNull View v, @NonNull Network model, int position) {
                networkName = v.findViewById(R.id.social_network_name);
                networkCategory = v.findViewById(R.id.social_network_category);
                networkPrivacy = v.findViewById(R.id.social_network_privacy);
                btn_join = v.findViewById(R.id.btn_join);

                networkName.setText(model.getNetworkName());
                networkCategory.setText(model.getCategory());
                networkPrivacy.setText(model.getPrivacy());

                btn_join.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dbRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                                for (DataSnapshot child : children) {
                                    networkId = child.getKey();
                                }
//                                Toast.makeText(SocialActivity.this, networkId, Toast.LENGTH_LONG).show();
                                intent = new Intent(SocialActivity.this, PostActivity.class);
                                intent.putExtra("networkKey", networkId);
                                startActivity(intent);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                    }
                });
            }
        };
        networkList.setAdapter(adapter);
    }

    private void initialize() {
        btn_add = findViewById(R.id.btn_social_create);
        btn_search = findViewById(R.id.btn_social_search);

        networkList = findViewById(R.id.social_network_list);
        searchField = findViewById(R.id.social_network_search);

        searchField.setVisibility(View.GONE);

        btn_add.setOnClickListener(this);
        btn_search.setOnClickListener(this);

        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference().child("networks");
    }

    @Override
    public void onClick(View view){
        if (view == btn_add) {
            intent = new Intent(SocialActivity.this, CreateNetworkActivity.class);
            startActivity(intent);
        } else if (view == btn_search) {
            searchNetwork();
        }
    }

    private void searchNetwork() {
        searchField.setVisibility(View.VISIBLE);

    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
