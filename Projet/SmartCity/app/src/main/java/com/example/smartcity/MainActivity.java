package com.example.smartcity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    FirebaseAuth fbAuth;
    FirebaseUser fbUser;
    DrawerLayout drawer;
    NavigationView navigationView;
    TextView hFname, hEmail;
    ImageView profilePic;
    MenuItem logout, login, profile, news, weather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initialize();

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_login, R.id.nav_about, R.id.nav_logout, R.id.nav_news, R.id.nav_weather, R.id.nav_profile, R.id.nav_location)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        fbAuth = FirebaseAuth.getInstance();
        fbUser = fbAuth.getCurrentUser();
        if (fbUser == null) {
            Toast.makeText(MainActivity.this, "Please log in first", Toast.LENGTH_LONG).show();
            drawer.openDrawer(Gravity.LEFT);
            logout.setVisible(false);
            profile.setVisible(false);
            news.setVisible(false);
            weather.setVisible(false);
        } else {
            logout.setVisible(true);
            login.setVisible(false);
            profile.setVisible(true);
            news.setVisible(true);
            weather.setVisible(true);

            fbUser = fbAuth.getCurrentUser();
            hFname.setText(fbUser.getDisplayName());
            hEmail.setText(fbUser.getEmail());
            Toast.makeText(MainActivity.this, "Welcome " + fbUser.getDisplayName(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void initialize() {
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);

        Menu menuNav = navigationView.getMenu();
        logout = menuNav.findItem(R.id.nav_logout);
        login = menuNav.findItem(R.id.nav_login);
        profile = menuNav.findItem(R.id.nav_profile);
        news = menuNav.findItem(R.id.nav_news);
        weather = menuNav.findItem(R.id.nav_weather);

        hFname = header.findViewById(R.id.header_fname);
        hEmail = header.findViewById(R.id.header_email);
        profilePic = header.findViewById(R.id.header_image);
    }
}
