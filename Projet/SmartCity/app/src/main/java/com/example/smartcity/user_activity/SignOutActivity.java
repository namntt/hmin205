package com.example.smartcity.user_activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartcity.MainActivity;
import com.example.smartcity.R;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SignOutActivity extends AppCompatActivity {
    FirebaseAuth fbAuth;
    FirebaseUser fbUser;
    MaterialTextView txtHome;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_out);

        fbAuth = FirebaseAuth.getInstance();
        fbUser = fbAuth.getCurrentUser();

        if (fbUser == null) {
            Toast.makeText(SignOutActivity.this, "Please log in first", Toast.LENGTH_LONG).show();
        } else {
            fbAuth.signOut();
        }

        txtHome = findViewById(R.id.home_link);
        txtHome.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                intent = new Intent(SignOutActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
