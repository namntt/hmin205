package com.example.smartcity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

import java.util.Locale;

public class OpenMapActivity extends AppCompatActivity {
    TextView location;
    MaterialButton btnMap;
    ImageView img;
    String TAG = "smartcity.LOCATION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_map);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialize();

        //locate
        //location.setText
        //get city image

        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pass location parameter to google map by this uri
                String uri = "geo:%f,%f";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });
    }
    public void initialize() {
        location = findViewById(R.id.map_location);
        btnMap = findViewById(R.id.btn_open_map);
        img = findViewById(R.id.cityImage);
    }
}

