package com.example.smartcity.user_activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.smartcity.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    String TAG = "com.example.SMARTCITY";
    int mYear, mMonth, mDay;
    List<String> genders;
    int spinnerPosition;
    ArrayAdapter<String> adapter;

    String userId;
    TextInputEditText fname, pw, loc, h, w, dob, email;
    Spinner gender;
    ToggleButton btn_edit;
    MaterialButton btn_cancel;

    FirebaseAuth fbAuth;
    DatabaseReference dbRef;
    FirebaseDatabase fDb;
    FirebaseUser fbUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fbAuth = FirebaseAuth.getInstance();
        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference().child("users");
        fbUser = fbAuth.getCurrentUser();

        initialize();

        if (fbUser != null) {
            userId = fbUser.getUid();
            dbRef.child(userId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    fname.setText(dataSnapshot.child("fullName").getValue(String.class));
                    pw.setText(dataSnapshot.child("password").getValue(String.class));
                    email.setText(dataSnapshot.child("email").getValue(String.class));
                    dob.setText(dataSnapshot.child("dob").getValue(String.class));
                    spinnerPosition = adapter.getPosition(dataSnapshot.child("gender").getValue(String.class));
                    gender.setSelection(spinnerPosition);
                    loc.setText(dataSnapshot.child("location").getValue(String.class));
                    h.setText(dataSnapshot.child("height").getValue(String.class));
                    w.setText(dataSnapshot.child("weight").getValue(String.class));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.d(TAG, "Error: " + databaseError.toString());
                }
            });
        } else {
            Toast.makeText(ProfileActivity.this, "Please log in first", Toast.LENGTH_LONG).show();
        }
    }

    public void toggleChange(View view){
        if (((ToggleButton) view).isChecked()) {
            fname.setEnabled(true);
            dob.setEnabled(true);
            pw.setEnabled(true);
            gender.setEnabled(true);
            loc.setEnabled(true);
            h.setEnabled(true);
            w.setEnabled(true);
        } else {
            fname.setEnabled(false);
            dob.setEnabled(false);
            pw.setEnabled(false);
            gender.setEnabled(false);
            loc.setEnabled(false);
            h.setEnabled(false);
            w.setEnabled(false);

            editProfile(view);
        }
    }

    public void editProfile(View view) {
        if (fbUser != null) {
            userId = fbUser.getUid();
            updateUser();
        } else {
            Toast.makeText(ProfileActivity.this, "Please log in first", Toast.LENGTH_LONG).show();
        }
    }

    public void updateUser() {
        String txtPw = pw.getText().toString().trim();
        String txtDob = dob.getText().toString().trim();
        String txtGender = gender.getSelectedItem().toString().trim();
        String txtCity = loc.getText().toString().trim();
        String txtH = h.getText().toString().trim();
        String txtW = w.getText().toString().trim();
        String txtFname = fname.getText().toString().trim();

        if(TextUtils.isEmpty(txtPw)){
            Toast.makeText(this, "Password must not be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if(txtPw.length() < 6){
            Toast.makeText(this, "Password too short", Toast.LENGTH_SHORT).show();
        }

        fbUser.updatePassword(txtPw).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "User password updated.");
                } else {
                    Log.d(TAG, "Password update error");
                }
            }
        });

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(txtFname).build();

        fbUser.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User profile updated.");
                        }
                    }
                });

        dbRef.child(userId).child("fullName").setValue(txtFname);
        dbRef.child(userId).child("dob").setValue(txtDob);
        dbRef.child(userId).child("password").setValue(txtPw);
        dbRef.child(userId).child("gender").setValue(txtGender);
        dbRef.child(userId).child("location").setValue(txtCity);
        dbRef.child(userId).child("height").setValue(txtH);
        dbRef.child(userId).child("weight").setValue(txtW);

        Toast.makeText(ProfileActivity.this, "Profile updated", Toast.LENGTH_LONG).show();
    }

    public void initialize() {
        email = findViewById(R.id.profile_email);
        fname = findViewById(R.id.profile_fname);
        dob = findViewById(R.id.profile_dob);
        pw = findViewById(R.id.profile_password);
        gender = findViewById(R.id.profile_gender);
        loc = findViewById(R.id.profile_city);
        h = findViewById(R.id.profile_height);
        w = findViewById(R.id.profile_weight);

        gender.setEnabled(false);

        btn_edit = findViewById(R.id.btn_profile_edit);
        btn_cancel = findViewById(R.id.btn_profile_cancel);

        genders = new ArrayList<>();
        genders.add("Male");
        genders.add("Female");
        genders.add("Other");

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, genders);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender.setAdapter(adapter);

        btn_edit.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        dob.setOnClickListener(this);
        gender.setOnItemSelectedListener(this);
    }

    public void pickDob() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                        dob.setText(date);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public void onClick(View view){
        if (view == btn_cancel) {
            String message = "Continue editing this profile?";
            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
            builder
                    .setMessage(message)
                    .setPositiveButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    })
                    .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }
        else if (view == btn_edit) {
            toggleChange(view);
        }
        else if (view == dob) {
            pickDob();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {}

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}
}
