package com.example.smartcity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.example.smartcity.data.Post;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class TrafficActivity extends AppCompatActivity {
    TextInputEditText ed;
    MaterialButton btt;

    FirebaseAuth fbAuth;
    DatabaseReference dbRef;
    FirebaseDatabase fDb;
    FirebaseUser fbUser;

    String subPostId;
    Post post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traffic);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btt = findViewById(R.id.btn_e);

        fDb = FirebaseDatabase.getInstance();
        dbRef = fDb.getReference().child("networks");
        fbAuth = FirebaseAuth.getInstance();
        fbUser = fbAuth.getCurrentUser();

        post = new Post("Welcome123", fbUser.getDisplayName());
        btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subPostId = dbRef.child("post").push().getKey();
                dbRef.child("networkId").child("post").child(subPostId).setValue(post);
            }
        });
    }
}
