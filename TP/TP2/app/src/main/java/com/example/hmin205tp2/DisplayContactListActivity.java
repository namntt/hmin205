package com.example.hmin205tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DisplayContactListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_contact_list);

        ListView listView = findViewById(R.id.contactList);
//        ArrayList<String> listContacts = new ArrayList<>();

        ArrayList<String> listContacts = new ArrayList<>(Arrays.asList(
                "PICARD Merle - 0572017487",
                "DAUBIGNE Bernard - 0425793922"
        ));

        try {
            byte[] bytes = new byte[1024];
            FileInputStream file = openFileInput("SavedContact");
            file.read(bytes);
            file.close();
            String content = new String(bytes);
            listContacts.add(content);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "error: " + e, Toast.LENGTH_SHORT).show();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.contact_list, R.id.contactText, listContacts);
        listView.setAdapter(adapter);
    }
}
