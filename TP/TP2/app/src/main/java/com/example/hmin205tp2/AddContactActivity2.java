package com.example.hmin205tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class AddContactActivity2 extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final ContactDbHelper dbHelper = new ContactDbHelper(AddContactActivity2.this);

        Button save = findViewById(R.id.sauvegarder);
        Button list = findViewById(R.id.ctliste);
        Button imp = findViewById(R.id.imp);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText eNom =  findViewById(R.id.nom);
                EditText ePnom =  findViewById(R.id.pnom);
                EditText ePhone =  findViewById(R.id.phone);

                String nom = eNom.getText().toString() + "\n";
                String pnom = ePnom.getText().toString();
                String phone = ePhone.getText().toString();

                if (dbHelper.isExisted(phone)){
                    Toast.makeText(getApplicationContext(), "Contact existed", Toast.LENGTH_SHORT).show();
                }
                else {
                    dbHelper.addContact(nom, pnom, phone);
                    Toast.makeText(getApplicationContext(), "Contact saved", Toast.LENGTH_SHORT).show();

                    eNom.setText("");
                    ePnom.setText("");
                    ePhone.setText("");
                }
            }
        });

        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddContactActivity2.this, DisplayContactListActivity2.class);
                startActivity(intent);
//                SQLiteDatabase db = dbHelper.getReadableDatabase();
//                dbHelper.dropTable(db);
//                Toast.makeText(getApplicationContext(), "Database cleared", Toast.LENGTH_SHORT).show();
            }
        });

        imp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> listContacts = new ArrayList<>(Arrays.asList(
                        "NGO Mai - 1234567",
                        "HWANG Tien - 00000",
                        "HUGO Victor - 01211121"
                ));

                try {
                    FileOutputStream savedFile = openFileOutput("ExistedContact", Context.MODE_PRIVATE);
                    savedFile.write((listContacts.toString().replaceAll("\\[|\\]", "").replaceAll(", ","\n")).getBytes());
                    savedFile.close();
                }
                catch(IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getApplicationContext(), "Contact imported", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(AddContactActivity2.this, ContactService.class);
                startService(intent);
                stopService(intent);
            }
        });
    }
}

