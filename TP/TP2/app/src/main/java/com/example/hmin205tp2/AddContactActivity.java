package com.example.hmin205tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class AddContactActivity extends AppCompatActivity {
    public static final String KEY = "com.example.hmin205tp2.KEY";
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button smt = findViewById(R.id.sauvegarder);
        Button contactList = findViewById(R.id.ctliste);

        final Intent intent = new Intent(AddContactActivity.this, DisplayContactListActivity.class);
        final ArrayList<String> contactArray = new ArrayList<>();

        smt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                EditText eNom =  findViewById(R.id.nom);
                EditText ePnom =  findViewById(R.id.pnom);
                EditText ePhone =  findViewById(R.id.phone);

                String nom = eNom.getText().toString() + " ";
                String pnom = ePnom.getText().toString() + " ";
                String phone = ePhone.getText().toString();

                String contact = nom + pnom + "- " + phone;
                contactArray.add(contact);
                Toast.makeText(getBaseContext(), "Contact saved", Toast.LENGTH_SHORT).show();

                eNom.setText("");
                ePnom.setText("");
                ePhone.setText("");
            }
        });

        contactList.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                try {
                    FileOutputStream savedFile = openFileOutput("SavedContact", Context.MODE_PRIVATE);
                    savedFile.write((contactArray.toString().replaceAll("\\[|\\]", "").replaceAll(", ","\n")).getBytes());
                    savedFile.close();

                }
                catch(IOException e) {
                    e.printStackTrace();
                }
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((TextView) findViewById(R.id.count)).setText(""+count++);
    }
}
