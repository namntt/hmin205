package com.example.hmin205tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class ContactDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "contact.db";
    private static final String TABLE_NAME = "contact_details";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_LNAME = "last_name";
    private static final String COLUMN_FNAME = "first_name";
    private static final String COLUMN_PHONE = "phone";
    private static final String CREATE_TABLE_QUERY = "create table " + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement,"
            + COLUMN_LNAME + " text not null,"
            + COLUMN_FNAME + " text not null,"
            + COLUMN_PHONE + " text not null" + ");";
    private static final String DROP_TABLE_QUERY = "drop table if exists " + TABLE_NAME + ";";

    public ContactDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTable(db);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void addContact(String lastName, String firstName, String phone) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_LNAME, lastName);
        values.put(COLUMN_FNAME, firstName);
        values.put(COLUMN_PHONE, phone);
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public ArrayList<HashMap<String, String>> getAllContact() {
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap<String, String>> contactList = new ArrayList<>();
        String query = "select last_name, first_name, phone from " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            HashMap<String, String> contact = new HashMap<>();
            contact.put("last_name", cursor.getString(cursor.getColumnIndex(COLUMN_LNAME)));
            contact.put("first_name", cursor.getString(cursor.getColumnIndex(COLUMN_FNAME)));
            contact.put("phone", cursor.getString(cursor.getColumnIndex(COLUMN_PHONE)));
            contactList.add(contact);
        }
        cursor.close();
        return contactList;
    }

    public boolean isExisted(String phone) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select * from " + TABLE_NAME + " where " + COLUMN_PHONE + " = " + "'" + phone + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void dropTable(SQLiteDatabase db) {
        db.execSQL(DROP_TABLE_QUERY);
        onCreate(db);
        db.close();
    }

}
