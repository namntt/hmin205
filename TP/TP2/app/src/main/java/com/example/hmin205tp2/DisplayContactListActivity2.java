package com.example.hmin205tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class DisplayContactListActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_contact_list2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        ArrayList<String> listContacts = new ArrayList<>(Arrays.asList(
//                "PICARD Merle - 0572017487",
//                "DAUBIGNE Bernard - 0425793922",
//                "HUGO Victor - 01211121"
//        ));

        ListView listView = findViewById(R.id.contactList2);
        ContactDbHelper dbHelper = new ContactDbHelper(DisplayContactListActivity2.this);
        ArrayList<HashMap<String, String>> contactList = dbHelper.getAllContact();
        ListAdapter adapter = new SimpleAdapter(DisplayContactListActivity2.this, contactList,
                R.layout.contact_list2, new String[]{"last_name", "first_name", "phone"},
                new int[]{R.id.txtNom, R.id.txtPnom, R.id.txtPhone});
        listView.setAdapter(adapter);
    }
}
