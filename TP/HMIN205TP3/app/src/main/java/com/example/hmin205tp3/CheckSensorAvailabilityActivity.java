package com.example.hmin205tp3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class CheckSensorAvailabilityActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_sensor_availability);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
