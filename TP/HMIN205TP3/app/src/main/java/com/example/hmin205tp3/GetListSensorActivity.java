package com.example.hmin205tp3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class GetListSensorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_list_sensor);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SensorManager sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        List<String> listSensor = new ArrayList<>();
        ListView listView = findViewById(R.id.sensorList);
        List<Sensor> sList = sm.getSensorList(Sensor.TYPE_ALL);

        for(Sensor s : sList) {
            listSensor.add(s.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.sensor_list, R.id.listText, listSensor);
        listView.setAdapter(adapter);
    }
}
