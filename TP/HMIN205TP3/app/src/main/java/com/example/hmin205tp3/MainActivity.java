package com.example.hmin205tp3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void getListSensor(View view) {
        intent = new Intent(MainActivity.this, GetListSensorActivity.class);
        startActivity(intent);
    }

    public void checkSensorAvailability (View vew) {
        intent = new Intent(MainActivity.this, CheckSensorAvailabilityActivity.class);
        startActivity(intent);
    }

    public void checkAccelerometer(View view) {
        intent = new Intent(MainActivity.this, CheckAccelerometerActivity.class);
        startActivity(intent);
    }

    public void checkDirection(View view) {

    }

    public void shakePhone(View view) {

    }

    public void checkProximity(View view) {

    }
}
