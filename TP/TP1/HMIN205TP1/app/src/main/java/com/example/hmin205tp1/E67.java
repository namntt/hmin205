package com.example.hmin205tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class E67 extends AppCompatActivity {
    public static final String MESSAGE = "Vos informations: ";
    public static final String EXTRA_MESSAGE = "com.example.hmin205tp1.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e67);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void validation(View view) {
        Intent intent = new Intent(this, DisplayInfo.class);

        EditText editNom = findViewById(R.id.nom);
        EditText editPnom = findViewById(R.id.pnom);
        EditText editAge = findViewById(R.id.age);
        EditText editDom = findViewById(R.id.domaine);

        String nom = editNom.getText().toString() + " ";
        String pnom = editPnom.getText().toString() + " ";
        String age = editAge.getText().toString() + " ans ";
        String dom = editDom.getText().toString();
        String message = MESSAGE + nom + pnom + age + dom;

        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
}
