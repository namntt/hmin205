package com.example.hmin205tp1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;

import java.util.Locale;

public class E3xml extends AppCompatActivity {
    public static final String MESSAGE = "Validez-vous ces informations?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e3xml);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void validation(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(E3xml.this);

        builder
                .setMessage(MESSAGE)
                .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        EditText editNom = findViewById(R.id.nom);
                        EditText editPnom = findViewById(R.id.pnom);
                        EditText editAge = findViewById(R.id.age);
                        EditText editDom = findViewById(R.id.domaine);

                        editNom.setBackgroundColor(getResources().getColor(android.R.color.holo_orange_light));
                        editPnom.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_bright));
                        editAge.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
                        editDom.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    public void changeToEnglish (View view) {
        setAppLocale("en");
        recreate();
    }

    public void changeToFrench (View view) {
        setAppLocale("fr");
        recreate();
    }

    public void setAppLocale(String localeCode) {
        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.JELLY_BEAN_MR1){
            config.setLocale(new Locale(localeCode.toLowerCase()));
        } else {
            config.locale = new Locale(localeCode.toLowerCase());
        }
        resources.updateConfiguration(config, dm);
    }
}
