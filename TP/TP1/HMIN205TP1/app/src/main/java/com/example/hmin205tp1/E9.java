package com.example.hmin205tp1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;

public class E9 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e9);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final CalendarView calendar = findViewById(R.id.calendar);

        calendar
                .setOnDateChangeListener(
                        new CalendarView
                                .OnDateChangeListener() {
                            @Override
                            public void onSelectedDayChange(@NonNull final CalendarView view,
                                                            int year, int month, int day) {
                                String date = "Aucun évenement au " + day + "/" + (month + 1) + "/" + year;

                                AlertDialog.Builder builder = new AlertDialog.Builder(E9.this);

                                builder
                                        .setMessage(date)
                                        .setPositiveButton("Ajout", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {
                                                addEvent(view);
                                            }
                                        })
                                        .setNegativeButton("Retour", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        })
                                        .show();
                            }
                        });
    }

    public void addEvent(View view) {
        Intent intent = new Intent(this, EmptyActivity.class);
        startActivity(intent);
    }
}
