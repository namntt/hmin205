package com.example.hmin205tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void exercise345(View view) {
        Intent intent = new Intent(MainActivity.this, E345.class);
        startActivity(intent);
    }

    public void exercise67(View view) {
        Intent intent = new Intent(MainActivity.this, E67.class);
        startActivity(intent);
    }

    public void exercise8(View view) {
        Intent intent = new Intent(MainActivity.this, E81.class);
        startActivity(intent);
    }

    public void exercise9(View view) {
        Intent intent = new Intent(MainActivity.this, E9.class);
        startActivity(intent);
    }
}
