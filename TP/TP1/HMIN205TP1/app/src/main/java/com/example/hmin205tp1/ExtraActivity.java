package com.example.hmin205tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ExtraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extra);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void call(View view) {
        EditText phoneText = findViewById(R.id.phoneNum);
        String num = phoneText.getText().toString();
        //String num = "6789";

        Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + num));
        startActivity(callIntent);
    }
}
