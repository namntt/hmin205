package com.example.hmin205tp1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class E3java extends AppCompatActivity {
    public static final String MESSAGE = "Validez-vous ces informations?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RelativeLayout relativeLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(130, 150, 100, 0);

        RelativeLayout.LayoutParams layoutIntro = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams layoutLbNom = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams layoutLbPnom = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams layoutLbAge = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams layoutLbDomaine = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        RelativeLayout.LayoutParams layoutNom = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams layoutPnom = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams layoutAge = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams layoutDomaine = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        RelativeLayout.LayoutParams layoutVerifier = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        TextView textIntro = new TextView(this);
        textIntro.setText("Java Version");
        textIntro.setTextSize(30);
        layoutIntro.leftMargin = 210;

        TextView lbNom = new TextView(this);
        lbNom.setText("Nom");
        lbNom.setTextSize(20);
        layoutLbNom.topMargin = 300;

        TextView lbPnom = new TextView(this);
        lbPnom.setText("Prenom");
        lbPnom.setTextSize(20);
        layoutLbPnom.topMargin = 400;

        TextView lbAge = new TextView(this);
        lbAge.setText("Age");
        lbAge.setTextSize(20);
        layoutLbAge.topMargin = 500;

        TextView lbDomaine = new TextView(this);
        lbDomaine.setText("Domaine");
        lbDomaine.setTextSize(20);
        layoutLbDomaine.topMargin = 600;

        EditText nom = new EditText(this);
        nom.setId(R.id.txtNom);
        nom.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        nom.setWidth(500);
        nom.setHeight(120);
        layoutNom.setMargins(260, 261, 0, 0);

        EditText pnom = new EditText(this);
        pnom.setId(R.id.txtPnom);
        nom.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        pnom.setWidth(500);
        pnom.setHeight(120);
        layoutPnom.setMargins(260, 363, 0, 0);

        EditText age = new EditText(this);
        age.setId(R.id.txtAge);
        age.setInputType(InputType.TYPE_CLASS_NUMBER);
        age.setWidth(500);
        age.setHeight(120);
        layoutAge.setMargins(260, 465, 0, 0);

        EditText domaine = new EditText(this);
        domaine.setId(R.id.txtDomaine);
        domaine.setWidth(500);
        domaine.setHeight(120);
        layoutDomaine.setMargins(260, 567, 0, 0);

        Button verify = new Button(this);
        verify.setId(R.id.buttonVerify);
        verify.setText("Verifier");
        layoutVerifier.setMargins(530, 700, 0, 0);
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                AlertDialog.Builder builder = new AlertDialog.Builder(E3java.this);

                builder
                        .setMessage(MESSAGE)
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                EditText editNom = findViewById(R.id.txtNom);
                                EditText editPnom = findViewById(R.id.txtPnom);
                                EditText editAge = findViewById(R.id.txtAge);
                                EditText editDom = findViewById(R.id.txtDomaine);

                                editNom.setBackgroundColor(getResources().getColor(android.R.color.holo_orange_light));
                                editPnom.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_bright));
                                editAge.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
                                editDom.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
                            }
                        })
                        .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                        .show();
            }
        });

        relativeLayout.addView(textIntro, layoutIntro);
        relativeLayout.addView(lbNom, layoutLbNom);
        relativeLayout.addView(lbPnom, layoutLbPnom);
        relativeLayout.addView(lbAge, layoutLbAge);
        relativeLayout.addView(lbDomaine, layoutLbDomaine);
        relativeLayout.addView(nom, layoutNom);
        relativeLayout.addView(pnom, layoutPnom);
        relativeLayout.addView(age, layoutAge);
        relativeLayout.addView(domaine, layoutDomaine);
        relativeLayout.addView(verify, layoutVerifier);

        setContentView(relativeLayout, layoutParams);
    }
}
