package com.example.hmin205tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class E81 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e81);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ListView listView = findViewById(R.id.hourList);
        String[] listItem = getResources().getStringArray(R.array.array_hours);
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.hour_list, R.id.hourText, listItem);
        listView.setAdapter(adapter);
    }
}
