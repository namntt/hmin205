package com.example.hmin205tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class E345 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e345);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void changeToXMLVersion(View view){
        Intent intent = new Intent(this, E3xml.class);
        startActivity(intent);
    }

    public void changeToJavaVersion(View view){
        Intent intent = new Intent(this, E3java.class);
        startActivity(intent);
    }
}
