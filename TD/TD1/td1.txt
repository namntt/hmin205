Exercise 1:
1 observer le code
2 groupe plusieur vues dans un layout 
	EditText et = new EditText(this)
	LinearLayout ly = new LinearLayout(this)
	ly.setOrientation(ly.Vertical)
	ly.add(tv)
	ly.add(et)
	SetContentView(ly)

3 difference: on a la vue declaratif (defini dans xml), pas programmatif (defini dans java)
4 ajouter element <EditText> sous <TextView>

Exercise 2:
- Difference: la vue declaratif est separee a 2 fichiers: resource et layout (pas donner text directement mais par un bouton - capture referent)
- Ajout un element TextView avec contenu volou, pour modifier, ajouter element EditText

Exercise 3
- layout 1: on peut met 1 a cote les autre horizontal s'il y a un bouton existe deja, 'fill-parent'
			3 boutons, text = "Button x", wrap_content
			le 3: layout_weight = "1" - tous les espace qui reste
- layout 2: RelativeLayout - pas linear, utilisateur mets n'importe qu'il veut
			TextView: layout_below: below button3
			alignParentRight: right parent, alignTop, toRightOf, ...
			inconvenient: tous sont depends --> changer 1, changer tous
- layout 3: TableLayout = table with column and row, define column by TableRow
			padding, layout_span
			<View> : create a line
			
Exercise 4:
- Toast: display text in serveral seconds
- makeText takes 3 arguments(present object, message, ...)
- keep text displaying: define TextView in layout and findviewbyid in java, add TextView at Toast 
- language problem: define message in resource and use in java code
- appui long: onLongClick method

Exercise 5:
- 